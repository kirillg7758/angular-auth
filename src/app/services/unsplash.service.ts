import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UnsplashService {

  constructor(
    private http: HttpClient
  ) { }

  randomPhoto(): Observable<any> {
    return this.http.get(this.generateUnsplashUri('/photos/random'))
  }

  private generateUnsplashUri(uri: string): string {
    return `${environment.unsplash.uri}${uri}/?client_id=${environment.unsplash.accessKey}`;
  }
}
