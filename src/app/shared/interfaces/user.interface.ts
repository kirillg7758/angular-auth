export interface IUser {
  id: string;
  firstName: string;
  lastName: string;
  dateOfBirthDay: Date | string;
  codeCountry: string;
  phone: string;
  email: string;
  password?: string;
  avatarUri: string;
  isEmailConfirmed: boolean;
}

export interface IUserCreate extends IUser {
  password: string;
  repeatPassword: string;
  confirmEmail: boolean;
}

export interface IUserSmall {
  id: string;
  firstName: string;
  lastName: string;
  avatarUri: string;
}

export interface IUserLogin {
  email: string;
  password: string;
  rememberMe: boolean;
}

export interface IUserSignUp {
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  dateOfBirthDay: Date | string;
  codeCountry: string;
  phone: string;
  avatar: File;
}
