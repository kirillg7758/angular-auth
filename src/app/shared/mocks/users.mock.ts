import {IUser} from '../interfaces/user.interface';

export const usersMock: IUser[] = [
  {
    id: 'user-1',
    firstName: 'Evgenia',
    lastName: 'Rudich',
    dateOfBirthDay: new Date(2001, 5, 12),
    codeCountry: '+380',
    phone: '501232123',
    email: 'admin@gmail.com',
    password: 'Qwerty1',
    isEmailConfirmed: true,
    avatarUri: 'assets/avatars/avatar-1.png',
  },
  {
    id: 'user-2',
    firstName: 'Kirill',
    lastName: 'Gerasimenko',
    dateOfBirthDay: new Date(2001, 2, 7),
    codeCountry: '+380',
    phone: '501232123',
    email: 'gera@gmail.com',
    password: 'Qwerty1',
    isEmailConfirmed: true,
    avatarUri: 'assets/avatars/avatar-2.jpg',
  },
];
