import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PasswordFieldComponent} from './password-field.component';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {AnimationModule} from '../animation/animation.module';



@NgModule({
  declarations: [PasswordFieldComponent],
  exports: [
    PasswordFieldComponent
  ],
    imports: [
        CommonModule,
        MatInputModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatIconModule,
        AnimationModule
    ]
})
export class PasswordFieldModule { }
