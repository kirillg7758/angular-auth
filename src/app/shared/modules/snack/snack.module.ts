import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SnackComponent} from './snack.component';
import {AnimationModule} from '../animation/animation.module';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  declarations: [SnackComponent],
  imports: [
    CommonModule,
    MatIconModule,
    AnimationModule
  ],
  providers: [AnimationModule]
})
export class SnackModule { }
