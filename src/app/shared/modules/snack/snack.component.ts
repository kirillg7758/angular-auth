import {Component, Inject} from '@angular/core';
import {MAT_SNACK_BAR_DATA} from '@angular/material/snack-bar';
import {ISnackData} from '../../services/snack.service';

@Component({
  selector: 'auth-snack',
  templateUrl: './snack.component.html',
  styleUrls: ['./snack.component.scss'],
})
export class SnackComponent {
  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: ISnackData
  ) {}
}
