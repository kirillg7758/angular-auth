import {Directive, Input} from '@angular/core';

@Directive({
  selector: 'step'
})
export class StepDirective {
  @Input() isValid = true;

  constructor() { }

}
