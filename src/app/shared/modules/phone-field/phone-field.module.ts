import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhoneFieldComponent } from './phone-field.component';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [PhoneFieldComponent],
  exports: [
    PhoneFieldComponent
  ],
    imports: [
        CommonModule,
        MatInputModule,
        ReactiveFormsModule
    ]
})
export class PhoneFieldModule { }
