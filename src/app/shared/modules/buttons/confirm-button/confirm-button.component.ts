import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'confirm-button',
  templateUrl: './confirm-button.component.html',
  styleUrls: ['./confirm-button.component.scss']
})
export class ConfirmButtonComponent implements OnInit {
  @Input() value = 'Confirm';
  @Input() disabled = false;
  @Input() hovered = false;
  @Input() pressed = false;

  @Output() clickEvent = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
