import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'submit-button',
  templateUrl: './submit-button.component.html',
  styleUrls: ['./submit-button.component.scss']
})
export class SubmitButtonComponent implements OnInit {
  @Input() value = 'Submit';
  @Input() disabled = false;
  @Input() hovered = false;
  @Input() pressed = false;

  @Output() submitEvent = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
