import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubmitButtonComponent } from './submit-button/submit-button.component';
import { CancelButtonComponent } from './cancel-button/cancel-button.component';
import { MatButtonModule } from '@angular/material/button';
import { ConfirmButtonComponent } from './confirm-button/confirm-button.component';



@NgModule({
  declarations: [SubmitButtonComponent, CancelButtonComponent, ConfirmButtonComponent],
  exports: [
    CancelButtonComponent,
    SubmitButtonComponent,
    ConfirmButtonComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule
  ],
})
export class ButtonsModule { }
