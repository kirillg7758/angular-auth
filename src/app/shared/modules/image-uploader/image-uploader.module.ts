import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageUploaderComponent } from './image-uploader.component';
import {ButtonsModule} from '../buttons/buttons.module';
import {MatButtonModule} from '@angular/material/button';
import {AnimationModule} from '../animation/animation.module';



@NgModule({
    declarations: [ImageUploaderComponent],
    exports: [
        ImageUploaderComponent
    ],
  imports: [
    CommonModule,
    ButtonsModule,
    MatButtonModule,
    AnimationModule
  ]
})
export class ImageUploaderModule { }
