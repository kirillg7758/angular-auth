import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.scss']
})
export class ImageUploaderComponent implements OnInit {
  @Output() upload: EventEmitter<File> = new EventEmitter<File>();
  selectedFile!: File;
  selectedFileSrc!: string;
  reader = new FileReader();

  constructor(
  ) { }

  ngOnInit(): void {
  }

  fileChanged(event: any): void {
    this.selectedFile = event.target.files[0];

    this.reader.readAsDataURL(this.selectedFile);
    this.reader.onload = ({ target }: ProgressEvent<FileReader>): void => {
      this.selectedFileSrc = target?.result as string;
    };
    this.upload.emit(this.selectedFile);
  }
}
