import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { LottieModule } from 'ngx-lottie';
import lottie, { LottiePlayer } from 'lottie-web';
import { AnimateIconComponent } from './animate-icon/animate-icon.component';
import { AnimateIconWrapperComponent } from './animate-icon-wrapper/animate-icon-wrapper.component';
import { AnimationLinkComponent } from './animation-link/animation-link.component';
import {MatButtonModule} from '@angular/material/button';
import {RouterModule} from '@angular/router';

export function playerFactory(): LottiePlayer {
  return lottie;
}

@NgModule({
  declarations: [
    AnimateIconComponent,
    AnimateIconWrapperComponent,
    AnimationLinkComponent,
  ],
  exports: [
    AnimateIconComponent,
    AnimateIconWrapperComponent,
    AnimationLinkComponent,
  ],
  imports: [
    CommonModule,
    LottieModule.forRoot({player: playerFactory}),
    MatButtonModule,
    RouterModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AnimationModule { }
