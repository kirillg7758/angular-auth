import {Component, Input, OnInit} from '@angular/core';
import {AnimateIconColor} from '../animate-icon/animate-icon.component';
import linkAnimation from '../../../animations/link.animation';

@Component({
  selector: 'animation-link',
  templateUrl: './animation-link.component.html',
  styleUrls: ['./animation-link.component.scss'],
  animations: [linkAnimation]
})
export class AnimationLinkComponent implements OnInit {
  @Input() color: AnimateIconColor = 'accent';
  @Input() iconColor: AnimateIconColor = 'light';
  @Input() iconName: string = '';
  @Input() linkText: string = '';
  @Input() routerLinkPath: string | string[]  = [];

  isOpen = false;

  constructor() { }

  ngOnInit(): void {
  }

  open(): void {
    this.isOpen = true;
  }

  close(): void {
    this.isOpen = false;
  }
}
