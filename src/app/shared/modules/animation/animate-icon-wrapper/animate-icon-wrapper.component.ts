import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AnimationOptions} from 'ngx-lottie';
import {AnimateIconColor, AnimateIconComponent} from '../animate-icon/animate-icon.component';

@Component({
  selector: 'animate-icon-wrapper',
  templateUrl: './animate-icon-wrapper.component.html',
  styleUrls: ['./animate-icon-wrapper.component.scss']
})
export class AnimateIconWrapperComponent implements OnInit {
  @Input() iconName: string = '';
  @Input() options: AnimationOptions = {};
  @Input() color: AnimateIconColor = 'dark';
  @Input() width = '24px';
  @Input() height = '24px';
  @Input() withHover = false;
  @Input() withSmSize = false;
  @Input() isActive = false;
  @Input() disabled = false;
  @Input() withStroke = false;

  @Output() animateWasCreated = new EventEmitter<boolean>();
  @Output() domLoaded = new EventEmitter<any>();

  @ViewChild(AnimateIconComponent) icon!: AnimateIconComponent;

  constructor() { }

  ngOnInit(): void {
  }

  onMouseEnter(): void {
    this.icon?.play();
  }

  onMouseLeave(): void {
    this.icon?.stop();
  }
}
