export const convertToFormDataUtil = (values: Record<string, any>): FormData => {
  const data = new FormData();
  const valuesAsArray: any[] = Object.keys(values);
  valuesAsArray.forEach(key => values[key] && data.append(key, values[key]));
  return data;
};
