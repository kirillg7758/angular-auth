import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AnimationModule} from './modules/animation/animation.module';
import {ButtonsModule} from './modules/buttons/buttons.module';
import {ImageUploaderModule} from './modules/image-uploader/image-uploader.module';
import {PasswordFieldModule} from './modules/password-field/password-field.module';
import {PhoneFieldModule} from './modules/phone-field/phone-field.module';
import {StepperModule} from './modules/stepper/stepper.module';
import {SnackModule} from './modules/snack/snack.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AnimationModule,
    ButtonsModule,
    ImageUploaderModule,
    PasswordFieldModule,
    PhoneFieldModule,
    StepperModule,
    SnackModule,
  ],
  exports: [
    AnimationModule,
    ButtonsModule,
    ImageUploaderModule,
    PasswordFieldModule,
    PhoneFieldModule,
    StepperModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
