import { Component, OnInit } from '@angular/core';
import {ProfileService} from "../services/profile.service";

@Component({
  selector: 'app-home',
  templateUrl: './home-root.component.html',
  styleUrls: ['./home-root.component.scss']
})
export class HomeRootComponent implements OnInit {

  constructor(
    private profileService: ProfileService
  ) { }

  ngOnInit(): void {
  }

  logout(): void {
    this.profileService.logout();
  }

}
