import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {equalValidator} from '../../shared/validators/equal.validator';
import {convertToFormDataUtil} from '../../shared/utils/convert-to-form-data.util';
import {StepperComponent} from '../../shared/modules/stepper/stepper.component';
import {ProfileService} from '../../services/profile.service';
import {IUserSignUp} from '../../shared/interfaces/user.interface';

@Component({
  selector: 'auth-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  @ViewChild(StepperComponent) stepper: any;
  personalInformationForm!: FormGroup;
  additionalInformationForm!: FormGroup;
  avatarFile: File | null = null;

  get isReadyForRequest(): boolean {
    return this.personalInformationForm.valid && this.additionalInformationForm.valid && !!this.avatarFile;
  }

  get showSubmitButton(): boolean {
    return this.isReadyForRequest && this.stepper?.isLastSelectedStep;
  }

  constructor(
    private profileService: ProfileService
  ) { }

  ngOnInit(): void {
    /** TODO: Cleared initial values */
    this.personalInformationForm = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      dateOfBirthDay: new FormControl(null, [Validators.required]),
    });

    this.additionalInformationForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      codeCountry: new FormControl(''),
      phone: new FormControl(''),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      repeatPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
    }, { validators: equalValidator('password', 'repeatPassword') });
  }

  uploadFile(avatar: File): void {
    this.avatarFile = avatar;
  }

  submit(): void {
    const values: IUserSignUp = {
      ...this.personalInformationForm.value,
      ...this.additionalInformationForm.value,
      avatar: this.avatarFile
    };
    const body = convertToFormDataUtil(values);
    body.forEach(item => console.log(item));
    console.log(values);

    this.profileService.sign_up(values);
  }

}
