import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthRootComponent } from './auth-root.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';
import { MatTabsModule } from '@angular/material/tabs';
import { AuthTabsComponent } from './components/auth-tabs/auth-tabs.component';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {PhoneFieldModule} from '../shared/modules/phone-field/phone-field.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ButtonsModule} from '../shared/modules/buttons/buttons.module';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {StepperModule} from '../shared/modules/stepper/stepper.module';
import {MatButtonModule} from '@angular/material/button';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {PasswordFieldModule} from '../shared/modules/password-field/password-field.module';
import {ImageUploaderModule} from '../shared/modules/image-uploader/image-uploader.module';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";


@NgModule({
  declarations: [AuthRootComponent, LoginComponent, SignUpComponent, AuthTabsComponent],
    imports: [
        CommonModule,
        AuthRoutingModule,
        MatTabsModule,
        MatIconModule,
        MatInputModule,
        MatRadioModule,
        PhoneFieldModule,
        FormsModule,
        ReactiveFormsModule,
        ButtonsModule,
        MatCheckboxModule,
        MatChipsModule,
        StepperModule,
        MatButtonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        PasswordFieldModule,
        ImageUploaderModule,
        MatProgressSpinnerModule,
    ]
})
export class AuthModule { }
