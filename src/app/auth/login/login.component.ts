import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {ProfileService} from '../../services/profile.service';

@Component({
  selector: 'auth-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form!: FormGroup;

  isSaving = false;

  get isValidForm(): boolean {
    return this.form.valid && this.form.touched;
  }

  constructor(
    private profileService: ProfileService
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm(): void {
    /** TODO: Cleared initial values */
    this.form = new FormGroup({
      email: new FormControl('gera@gmail.com', [Validators.required, Validators.email]),
      password: new FormControl('Qwerty1', [Validators.required, Validators.minLength(6)]),
      rememberMe: new FormControl(false)
    });
  }

  login(): void {
    this.isSaving = true;

    this.profileService.login(this.form.value);
    setTimeout(() => this.isSaving = false, 1000);
  }

}
