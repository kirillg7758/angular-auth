import { Component, OnInit } from '@angular/core';
import {UnsplashService} from "../../../services/unsplash.service";

@Component({
  selector: 'auth-tabs',
  templateUrl: './auth-tabs.component.html',
  styleUrls: ['./auth-tabs.component.scss']
})
export class AuthTabsComponent implements OnInit {
  backgroundImage: string = '';
  isLoading = true;

  constructor(
    private unsplashService: UnsplashService
  ) { }

  ngOnInit(): void {
    this.unsplashService.randomPhoto().subscribe(res => {
      this.backgroundImage = res.urls.full + `&w=${window.innerWidth}&dpr=2`;
    });
  }

  loadedData(event: Event): void {
    this.isLoading = false;
  }
}
